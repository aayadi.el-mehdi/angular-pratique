import { Component } from '@angular/core';
import set = Reflect.set;
import {promise} from 'selenium-webdriver';
import {reject} from 'q';
import {stringifyElement} from '@angular/platform-browser/testing/src/browser_util';

@Component({
  selector: 'app-root', // pour attribut [] // pour class .
  templateUrl: './app.component.html',
  // template : '<p> .... </p>', je peux ecrire de cette façon
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  isAuth = false ;
  lastUpdate = new Date();

  appareils = [
    {
      nom : "machine à laver",
      status: "Allumé"
    },
    {
      nom : "machine à secher",
      status: "Eteint"
    },
    {
      nom : "machine à manger",
      status: "Allumé"
    }
  ]


  // constructor executer apres la création de ce component
  constructor(){
    setTimeout(
      ()=>{
        this.isAuth=true;
      }, 4000
    );
  }

  onAllumer(){
    console.log("On allume tout !! ")
  }

  posts = [
    {
      titre: "Poste 1",
      contenu: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
        "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
        "nisi ut aliquip ex ea commodo consequat.",
      jaime: 0 ,
      jaimepas:0,
      date_creation : null
    },
    {
      titre: "Poste 2",
      contenu: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
        "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
        "nisi ut aliquip ex ea commodo consequat.",
      jaime: 0 ,
      jaimepas:0,
      date_creation : null
    },
    {
      titre: "Poste 3",
      contenu: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
        "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
        "nisi ut aliquip ex ea commodo consequat.",
      jaime: 0 ,
      jaimepas:0,
      date_creation : null
    }
  ]

}
