import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  //info:any sois string date ou n'importe
  info={
    nom:"elmehdi",
    email:"elmehdi@gmail.com",
    tel:"0751423477"
  }

  commentaire=[
    {date:new Date() , message:"A"},
    {date:new Date() , message:"B"},
    {date:new Date() , message:"C"}
  ];

  commentaire_saisie = {
    date:new Date(),
    message:""
  }

  onaddcommentaire(c){
    this.commentaire_saisie.message = c.message;

    this.commentaire.push(this.commentaire_saisie);

    this.commentaire_saisie = {
      date:new Date(),
      message:""
    }

    this.commentaire_saisie.message="";
  }
}
