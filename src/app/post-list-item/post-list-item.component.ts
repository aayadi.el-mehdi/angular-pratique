import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

  @Input() title : string ;
  @Input() content : string;
  @Input() loveit : number;
  @Input() dontloveit : number;
  @Input() created_at : Date;


  constructor() {

  }

  ngOnInit() {
  }

  getColor(){
    if(this.loveit > this.dontloveit)
      return 'green';
    else if (this.loveit < this.dontloveit)
      return 'red';
  }

  onClickLove(){
    this.loveit ++;
  }

  onClickDontLove(){
    this.dontloveit ++
  }





}
