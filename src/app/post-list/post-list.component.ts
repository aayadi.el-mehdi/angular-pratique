import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  @Input() title : string ;
  @Input() content : string;
  @Input() loveit : number;
  @Input() dontloveit : number;
  @Input() created_at : Date;

  constructor() {
    this.created_at = new Date();
  }

  ngOnInit() {
  }

}
